const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const config = require('../config');
const User = require('../model/User');

const createRouter = () => {
    router.post('/', async function (req, res) {

        try {
            const user = await User.create({
                username: req.body.username,
                password: req.body.password,
                token: ''
            });

            res.status(200).send({auth: true, username: user.username, message: 'successfully created'});
        } catch (err) {
            return res.status(400).send({error: 'bad request', message: err.message});
        }
    });

    router.post('/sessions', async function (req, res) {

        try {
            const user = await User.findOne({username: req.body.username});

            const passwordIsValid = await bcrypt.compare(req.body.password, user.password);
            if (!passwordIsValid) return res.status(401).send({error: 'password not valid'});

            const token = jwt.sign({id: user._id}, config.token.secret, {
                expiresIn: config.token.lifetime
            });

            user.token = token;
            await user.save();

            res.status(200).send({username: user.username, token: token})
        } catch (err) {
            return res.status(401).send({error: 'Username not found'})
        }
    });

    return router;
};

module.exports = createRouter;