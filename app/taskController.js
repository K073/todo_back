const express = require('express');
const router = express.Router();

const Task = require('../model/Task');
const auth = require('../middleware/auth');

const createRouter = () => {
    router.post('/', auth, async function (req, res) {
        const productData = req.body;
        productData.user = req.userId;

        try {
            const task = await Task.create({
                user: req.body.user,
                title: req.body.title,
                description: req.body.description,
                status: req.body.status
            });

            console.log(task);

            res.status(200).send({auth: true, title: task.title, user: task.user});
        } catch (err) {
            return res.status(400).send({error: 'Token not present', message: err.message});
        }
    });

    router.get('/', auth, async function (req, res) {

        try {
            const allTask = await Task.find({user: req.userId});

            res.status(200).send({auth: true, allTask});
        } catch (err) {
            return res.status(401).send({error: 'Task not found', message: err.message})
        }
    });

    router.put('/:id', auth, async function (req, res) {
        try {
            let data = {
                title: req.body.title,
                description: req.body.description,
                status: req.body.status
            };

            await Task.findByIdAndUpdate(req.params.id, data);

            res.status(200).send({auth: true, data});
        } catch (err) {
            return res.status(401).send({error: 'Task not found', message: err.message})
        }
    });

    router.delete('/:id', auth, async function (req, res) {

        try {
            await Task.findOneAndRemove({_id: req.params.id});

            res.status(200).send({auth: true, message: 'was removed'});
        } catch (err) {
            return res.status(401).send({error: 'Task not found', message: err.message})
        }
    });


    return router;
};

module.exports = createRouter;