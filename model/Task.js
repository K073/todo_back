const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaskSchema = new mongoose.Schema({
    user : {
        type: Schema.Types.ObjectId, ref:'User'
    },
    title:{
        type: String, required: true
    },
    description:{
        type: String
    },
    status : {
        type: String, required: true
    }
});



mongoose.model('Task', TaskSchema);

module.exports = mongoose.model('Task');