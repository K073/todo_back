const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_ROUND = 8;

const UserSchema = new mongoose.Schema({
    username: {
        type: String, required: true, unique: true
    },
    password: {
        type: String, required: true
    },
    token: {
        type: String
    }
});

UserSchema.pre('save', async function (next) {
    if (!this.isModified('password')) return next();

    const salt = await bcrypt.genSalt(SALT_ROUND);
    this.password = await bcrypt.hash(this.password, salt);
    next();
});

UserSchema.set('toJson', {
    transform: (doc, ret, options) => {
        delete ret.password;
        return ret;
    }
});

mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');